const baseUrl = "t.meoyun.com"; //域名
const miniappName = "云喵圈子-每个人的兴趣生活社区"; //站点标题 ，一般用于分享时的标题


const domain = 'https://' + baseUrl + "/api/"; //接口地址（无需更改）
export default {
	baseUrl: baseUrl,
	domain: domain,
	miniappName: miniappName
}
