#### 遵循Apache-2.0开源协议，免费商用，可去除版权信息

#### 如果对您有帮助，您可以点右上角 "Star" 支持一下，这样我们才有继续免费下去的动力，谢谢！

#### 扫描预览

<img src="https://www-meow.oss-cn-beijing.aliyuncs.com/2021/09/163141464152629.jpg" width = "200px" height = "200px" />

#### 功能架构：uniapp+vue+thinkphp6

#### 介绍

兴趣社区圈子论坛程序,后台服务端基于ThinkPHP6开发，是一套兴趣社区论坛系统，代码开源免费使用，如果觉得还不错，帮忙给个五星好评，使用过程
中如遇到安装或者其他问题，请联系我们进行解决。

ps:目前仅适配微信小程序端


码云：[点击访问](https://gitee.com/ym721/cloud-meow-uni)

#### 安装说明

1、如安装过程中出现404等问题，请配置nginx或Apache伪静态，配置教程：[点击访问](https://www.kancloud.cn/manual/thinkphp5/177576)

2、uniapp端下载后在根目录：utils/config.js中配置站点域名，运行到小程序即可


#### 软件架构

前端：uniapp

后端：ThinkPHP6


#### 加我微信，进技术交流群
<img src="https://www-meow.oss-cn-beijing.aliyuncs.com/2021/09/163125532152976.jpg" width = "200px" height = "260px" />

官网：[喵云官网](https://www.meoyun.com)

#### 承诺源码无加密，定制项目二开请联系微信